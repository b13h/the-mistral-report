#ifdef AMIGA
#include "AmigaInt.h"
#else

#ifdef WIN32
#include "Win32Int.h"
#else
#include <stdint.h>
#include <unistd.h>

#endif
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "Enums.h"
#include "Engine.h"
#include "Common.h"
#include "SpyTravel.h"
#include "CPackedFileReader.h"

struct Location {
	char *name;
	char *demonym;
	int distances[8];
};

struct Agent {
	char *name;
	int location;
	int alive;
	int givenClue;
	int turnToWait;
	int turnToDestination;
	int rescued;
	InfoProcessor infoProcessor;
};
struct Location locations[8];
struct Agent agents[8];
struct Agent player;
uint8_t nextVictim[8];

struct SpiesSession spiesSession;

void setAsRescued(int c) {
	agents[c].rescued = TRUE;
}

int hasBeenRescued(int c) {
	return agents[c].rescued;
}

uint8_t getNextVictim() {
	return nextVictim[spiesSession.victim];
}

int tickSpiesTurn() {
	int repeats;
	struct Agent *killer;
	struct Agent *victim;

	for (repeats = 1; repeats < 3; ++repeats) {
		if (spiesSession.love < 25) {
			spiesSession.love++;
		}

		if (spiesSession.rage < 25) {
			spiesSession.rage++;
		}

		if (spiesSession.nostalgia < 25) {
			spiesSession.nostalgia++;
		}

		if (spiesSession.volatility < 25) {
			spiesSession.volatility++;
		}
	}

	spiesSession.turn++;

	killer = &agents[spiesSession.killer];
	victim = &agents[spiesSession.victim];


	killer->turnToWait--;

	if (killer->turnToWait <= 0) {

		killer->turnToWait = 0;

		if (killer->turnToDestination <= 0) {

			int turns;
			int distance;

			killer->turnToWait = 5;

			if ( !victim->rescued) {
				victim->alive = FALSE;
			}

			spiesSession.victim = getNextVictim();

			if (spiesSession.victim == 255) {
				return kCrawlerGameOver;
			}

			turns = 1;
			distance =
					locations[player.location].distances[victim->location] / 255;
			killer->turnToDestination = (turns > distance) ? turns : distance;
			killer->location = victim->location;
		} else {
			killer->turnToDestination--;
		}
	}


	if (player.turnToDestination == 0) {

		if (killer->location == player.location
			&& killer->turnToDestination == 0) {
			killer->alive = FALSE;
			return 1;
		}
	} else {
		player.turnToDestination--;
	}

	return 0;
}

int getTurn() {
	return spiesSession.turn;
}

char dossiers[8][40 * 25];

void getFileTextContents(int c) {
	char filename[16];
	char suspectName[256];
	size_t fileSize;
	FILE *fileInput;

	getSuspectName(c, suspectName, 256);
	sprintf (&filename[0], "%s.txt", suspectName);
	memset (&dossiers[c][0], 0, (40 * 25));

	fileSize = sizeOfFile(filename);
	fileInput = openBinaryFileFromPath(filename);

	assert (fread(&dossiers[c][0], fileSize, 1, fileInput));
	fclose(fileInput);
}

void getClue() {
	agents[player.location].givenClue = TRUE;
}

char *getDenonym(int c) {
	return locations[c].demonym;
}

int isBanditPresent() {
	return player.location == agents[spiesSession.killer].location;
}

int isSuspectAlive() {
	return agents[player.location].alive;
}

int isSuspectRescued() {
	return agents[player.location].rescued;
}

size_t infoOnLiar(int source, char *buffer, size_t size) {
	if ( spiesSession.liar != source ) {
		return sprintf (buffer, "It's hard to trust %s.",
						agents[spiesSession.liar].name);
	} else {
		return sprintf (buffer, "For I know, %s should be very\ncareful...",
						agents[spiesSession.killer].name);
	}
}

size_t infoNextVictim(int source, char *buffer, size_t size) {
	if ( spiesSession.killer == source ) {
		return sprintf (buffer, "I feel my days are numbered.");
	} else {
		return sprintf (buffer, "For I know, %s should be very\ncareful.",
						agents[spiesSession.killer].name);
	}
}

size_t infoBloodOnStreets(int source, char *buffer, size_t size) {
	return sprintf (buffer, "Word on the streets tell me there will\nbe blood at %s.",
					 locations[agents[spiesSession.liar].location].name);
}

size_t infoOnTrustworthy(int source, char *buffer, size_t size) {

	if ( spiesSession.trustworthy == source ) {
		return sprintf (buffer, "I only trust myself, really.");

	} else {
		return sprintf (buffer, "For I know, %s should be very\ncareful.",
						agents[spiesSession.killer].name);
	}
}

size_t infoAccuseYou(int source, char *buffer, size_t size) {
	return sprintf (buffer,
					 "I'm certain this is a ploy - you're\nbehind this and I "
					 "will eventually be\nable to prove it.\nYou can't stop me. You'll regret this!");
}

size_t infoNothingToDoWithIt(int source, char *buffer, size_t size) {
	return sprintf (buffer, "I have nothing to do with that.");
}

size_t infoSomethingOddAt(int source, char *buffer, size_t size) {
	return sprintf (buffer, "You should check what's going on at\n%s.",
					 locations[agents[spiesSession.killer].location].name);
}

size_t infoOutOfThisLoop(int source, char *buffer, size_t size) {
	return sprintf (buffer, "I'm out of the loop on this.");
}

void initSpyGame() {
	int c = 0;
	int d = 0;
	int vals[10] = {10, 10, 10 , 10};
	int location;
	/*Memsetting, to init fields that must start as FALSE */
	memset (&locations[0], 0, sizeof(struct Location) * 8);
	memset (&agents[0], 0, sizeof(struct Agent) * 8);
	memset (&spiesSession, 0, sizeof(struct SpiesSession));

	locations[0].name = "Porto";
	locations[1].name = "Lisbon";
	locations[2].name = "Madrid";
	locations[3].name = "Barcelona";
	locations[4].name = "Frankfurt";
	locations[5].name = "Hamburg";
	locations[6].name = "Luxembourg";
	locations[7].name = "Brussels";

	locations[0].demonym = "portuguese";
	locations[1].demonym = "portuguese";
	locations[2].demonym = "castilian";
	locations[3].demonym = "catalan";
	locations[4].demonym = "germanic";
	locations[5].demonym = "germanic";
	locations[6].demonym = "luxembourger";
	locations[7].demonym = "belgian";

	locations[0].distances[0] = 0;
	locations[0].distances[1] = 313;
	locations[0].distances[2] = 560;
	locations[0].distances[3] = 1158;
	locations[0].distances[4] = 2162;
	locations[0].distances[5] = 1985;
	locations[0].distances[6] = 2000;
	locations[0].distances[7] = 1864;

	locations[1].distances[0] = 313;
	locations[1].distances[1] = 0;
	locations[1].distances[2] = 629;
	locations[1].distances[3] = 1248;
	locations[1].distances[4] = 2317;
	locations[1].distances[5] = 2138;
	locations[1].distances[6] = 2126;
	locations[1].distances[7] = 2018;

	locations[2].distances[0] = 560;
	locations[2].distances[1] = 629;
	locations[2].distances[2] = 0;
	locations[2].distances[3] = 618;
	locations[2].distances[4] = 1845;
	locations[2].distances[5] = 1668;
	locations[2].distances[6] = 1665;
	locations[2].distances[7] = 1547;

	locations[3].distances[0] = 1158;
	locations[3].distances[1] = 1248;
	locations[3].distances[2] = 618;
	locations[3].distances[3] = 0;
	locations[3].distances[4] = 1330;
	locations[3].distances[5] = 1159;
	locations[3].distances[6] = 1663;
	locations[3].distances[7] = 1274;

	locations[4].distances[0] = 2162;
	locations[4].distances[1] = 2317;
	locations[4].distances[2] = 1845;
	locations[4].distances[3] = 1330;
	locations[4].distances[4] = 0;
	locations[4].distances[5] = 187;
	locations[4].distances[6] = 220;
	locations[4].distances[7] = 397;

	locations[5].distances[0] = 1985;
	locations[5].distances[1] = 2138;
	locations[5].distances[2] = 1668;
	locations[5].distances[3] = 1159;
	locations[5].distances[4] = 187;
	locations[5].distances[5] = 0;
	locations[5].distances[6] = 109;
	locations[5].distances[7] = 293;

	locations[6].distances[0] = 2000;
	locations[6].distances[1] = 2126;
	locations[6].distances[2] = 1665;
	locations[6].distances[3] = 1163;
	locations[6].distances[4] = 220;
	locations[6].distances[5] = 109;
	locations[6].distances[6] = 0;
	locations[6].distances[7] = 212;

	locations[7].distances[0] = 1864;
	locations[7].distances[1] = 2018;
	locations[7].distances[2] = 1547;
	locations[7].distances[3] = 1274;
	locations[7].distances[4] = 397;
	locations[7].distances[5] = 293;
	locations[7].distances[6] = 212;
	locations[7].distances[7] = 0;

	/*Might seem odd to have the locations be same as the index, but the killer
	   * can travel around freely and is not bound by it's index.*/

	agents[0].name = "Sofia";
	agents[0].alive = TRUE;
	agents[0].location = 0;

	agents[1].name = "Ricardo";
	agents[1].alive = TRUE;
	agents[1].location = 1;

	agents[2].name = "Lola";
	agents[2].alive = TRUE;
	agents[2].location = 2;

	agents[3].name = "Pau";
	agents[3].alive = TRUE;
	agents[3].location = 3;

	agents[4].name = "Lina";
	agents[4].alive = TRUE;
	agents[4].location = 4;

	agents[5].name = "Elias";
	agents[5].alive = TRUE;
	agents[5].location = 5;

	agents[6].name = "Carmen";
	agents[6].alive = TRUE;
	agents[6].location = 6;

	agents[7].name = "Jean";
	agents[7].alive = TRUE;
	agents[7].location = 7;

	/* Init player separately, since he's not among the other agents */
	player.location = rand() % 8;
	player.rescued = TRUE;
	player.alive = TRUE;
	player.turnToDestination = 0;
	player.turnToWait = 0;

	/*Since the killer is the first one, we can select any option*/
	spiesSession.killer = rand() % 8;

	/*The only rule at this point is that the killer must not start in the same
	   * city as the player - otherwise, the game would be boring*/

	agents[spiesSession.killer].location = player.location;

	while (agents[spiesSession.killer].location == player.location) {
		agents[spiesSession.killer].location = rand() % 8;
	}

	spiesSession.victim = (spiesSession.killer + 1 + (rand() % 1)) % 8;
	spiesSession.liar = (spiesSession.killer + 3 + (rand() % 1)) % 8;
	spiesSession.innocent = (spiesSession.killer + 5 + (rand() % 1)) % 8;
	spiesSession.trustworthy = (spiesSession.killer + 7) % 8;

	agents[spiesSession.killer].rescued = TRUE;
	agents[spiesSession.killer].turnToDestination = 0;
	agents[spiesSession.killer].turnToWait = 0;
	agents[spiesSession.liar].rescued = TRUE;

	for (c = 0; c < 8; ++c) {
		getFileTextContents(c);

		switch (rand() % 3) {
			case 0:
				agents[c].infoProcessor = infoNextVictim;
				break;
			case 1:
				agents[c].infoProcessor = infoBloodOnStreets;
				break;
			case 2:
				agents[c].infoProcessor = infoOutOfThisLoop;
				break;
		}
	}

	agents[spiesSession.killer].infoProcessor = infoOnLiar;
	agents[spiesSession.victim].infoProcessor = infoOnTrustworthy;
	agents[spiesSession.innocent].infoProcessor = infoAccuseYou;
	agents[spiesSession.trustworthy].infoProcessor = infoNothingToDoWithIt;
	agents[spiesSession.liar].infoProcessor = infoSomethingOddAt;

	location = agents[spiesSession.killer].location;

	updateEmotionsStats(&vals[0]);

	{
		int previousCandidate = -1;
		uint8_t taken[8];
		memset (&taken[0], 0, sizeof(uint8_t) * 8);
		memset (&nextVictim[0], 255, sizeof(uint8_t) * 8);
		agents[spiesSession.killer].location = -1;
		taken[spiesSession.killer] = TRUE;

		for (c = 0; c < 8; ++c) {
			int begin = ((spiesSession.killer + (c) + rand()) % 8);
			int candidate = -1;

			for (d = 0; d < 8; ++d) {

				candidate = (begin + d + 1) % 8;

				if ((agents[candidate].location != location)
					&& !taken[candidate]) {

					taken[candidate] = TRUE;
					goto foundCandidate;
				}
			}

			nextVictim[previousCandidate] = -1;
			return;

			foundCandidate:
			location = agents[candidate].location;

			if (previousCandidate == -1 ) {
				spiesSession.victim = candidate;
				agents[spiesSession.killer].location = location;
				agents[spiesSession.killer].turnToDestination = locations[location].distances[agents[candidate].location] / 255;
				agents[spiesSession.killer].turnToWait = 0;
			} else {
				nextVictim[previousCandidate] = candidate;
			}
			previousCandidate = candidate;
		}
	}
}

size_t getDossierText(int c, char *buffer, size_t size) {
	size_t bufferUsage = 0;
	bufferUsage += sprintf (buffer + bufferUsage,
							 "Bio:\n%s\n", dossiers[c]);

	if (hasGivenClue(c)) {
		bufferUsage +=
				sprintf (buffer + bufferUsage, "\nIntel:\n");
		bufferUsage +=
				getSuspectClue(c, buffer + bufferUsage, size - bufferUsage);
	}

	return bufferUsage;
}

void getEmotionsStats(int *stats) {
	stats[0] = spiesSession.love;
	stats[1] = spiesSession.rage;
	stats[2] = spiesSession.nostalgia;
	stats[3] = spiesSession.volatility;
}

void updateEmotionsStats(int *stats) {
	spiesSession.love = stats[0];
	spiesSession.rage = stats[1];
	spiesSession.nostalgia = stats[2];
	spiesSession.volatility = stats[3];
}

int getPlayerLocation() {
	return player.location;
}

size_t getLocationName(int c, char *buffer, size_t size) {

	size_t bufferUsage = 0;

	bufferUsage += sprintf (buffer + bufferUsage, "%s",
							 locations[c].name);

	return bufferUsage;
}

size_t getSuspectName(int c, char *buffer, size_t size) {
	return sprintf (buffer, "%s", agents[c].name);
}

int accuse(int zeroBasedIndexAgent) {
	return spiesSession.killer == zeroBasedIndexAgent;
}

size_t getSuspectClue(int c, char *buffer, size_t size) {

	size_t bufferUsage = 0;
	agents[c].infoProcessor(c, buffer, size);

	return bufferUsage;
}

int hasGivenClue(int c) {
	return agents[c].givenClue;
}

size_t getDisplayStatusText(char *buffer, size_t size) {

	size_t bufferUsage = 0;
	int c = 0;
	int hasDeadAgents = FALSE;

	bufferUsage +=
			sprintf (buffer + bufferUsage, "MIA:\n");
	assert (bufferUsage < size);

	for (c = 0; c < 8; ++c) {
		struct Agent *agent = &agents[c];
		if (agent->alive && !agent->rescued) {
			struct Location *location = &locations[agent->location];
			bufferUsage += sprintf (buffer + bufferUsage,
									 "-%s: %s\n", agent->name, location->name);
			assert (bufferUsage < size);
		}

		if (!agent->alive) {
			hasDeadAgents = TRUE;
		}
	}

	if (hasDeadAgents) {
		bufferUsage += sprintf (buffer + bufferUsage,
								 "\nDead agents:\n");
		assert (bufferUsage < size);

		for (c = 0; c < 8; ++c) {
			struct Agent *agent = &agents[c];
			if (!agent->alive) {
				bufferUsage += sprintf (buffer + bufferUsage,
										 "-%s\n", agent->name);
				assert (bufferUsage < size);
			}
		}
	}

	return bufferUsage;
}

void goTo(int newLocation) {

	int turns = 1;
	int distance = locations[player.location].distances[newLocation] / 255;

	player.turnToDestination = (turns > distance) ? turns : distance;
	player.location = newLocation;
	player.turnToWait = 0;
}

int gameTurn() {

	int returnValue = tickSpiesTurn();

	if (returnValue) {
		return returnValue;
	}

	while (player.turnToDestination > 0) {
		returnValue = tickSpiesTurn();

		if (returnValue) {
			return returnValue;
		}
	}

	return 0;
}

int mapGameTick() {
	return gameTurn();
}
