#ifndef SPYTRAVEL_H
#define SPYTRAVEL_H

int mapGameTick();

size_t getDisplayStatusText(char *buffer, size_t size);

size_t getDossierText(int suspect, char *buffer, size_t size);

int isBanditPresent();

int isSuspectAlive();

int isSuspectRescued();

int getTurn();

void getClue();

char *getDenonym(int c);

int getPlayerLocation();

size_t getLocationName(int c, char *buffer, size_t size);

int hasGivenClue(int c);

int hasBeenRescued(int c);

size_t getSuspectName(int c, char *buffer, size_t size);

size_t getSuspectClue(int c, char *buffer, size_t size);

int accuse(int zeroBasedIndexAgent);

void updateEmotionsStats(int *stats);

void getEmotionsStats(int *stats);

void initSpyGame();

typedef size_t ( *InfoProcessor )(int source, char *buffer, size_t size);

void setAsRescued(int c);

struct SpiesSession {
	int love;
	int rage;
	int nostalgia;
	int volatility;
	int liar;
	int victim;
	int killer;
	int trustworthy;
	int innocent;
	int turn;
};

#endif
