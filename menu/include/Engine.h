#ifndef ENGINE_H
#define ENGINE_H

typedef int32_t ( *InitStateCallback )(int32_t tag);

typedef void ( *InitialPaintCallback )();

typedef void ( *RepaintCallback )();

typedef int32_t ( *TickCallback )(int32_t tag, void *data);

typedef void ( *UnloadStateCallback )();

#define kNonExpiringPresentationState 0xFFFF
#define kDefaultPresentationStateInterval 2000
#define kMenuStateUnchanged -1


extern InitStateCallback initStateCallback;
extern InitialPaintCallback initialPaintCallback;
extern RepaintCallback repaintCallback;
extern TickCallback tickCallback;
extern UnloadStateCallback unloadStateCallback;
extern int isRunning;

extern long timeUntilNextState;
extern enum EPresentationState currentPresentationState;
extern struct Bitmap *currentBackgroundBitmap;

extern uint8_t cursorPosition;
extern int32_t nextNavigationSelection;
extern int32_t menuStateToReturn;

int menuTick(long ms);

int32_t MainMenu_initStateCallback(int32_t tag);

void MainMenu_initialPaintCallback();

void MainMenu_repaintCallback();

int32_t MainMenu_tickCallback(int32_t tag, void *data);

void MainMenu_unloadStateCallback();

int32_t Crawler_initStateCallback(int32_t tag);

void Crawler_initialPaintCallback();

void Crawler_repaintCallback();

int32_t Crawler_tickCallback(int32_t tag, void *data);

void Crawler_unloadStateCallback();

int32_t HelpScreen_initStateCallback(int32_t tag);

void HelpScreen_initialPaintCallback();

void HelpScreen_repaintCallback();

int32_t HelpScreen_tickCallback(int32_t tag, void *data);

void HelpScreen_unloadStateCallback();

int32_t Interrogation_initStateCallback(int32_t tag);

void Interrogation_initialPaintCallback();

void Interrogation_repaintCallback();

int32_t Interrogation_tickCallback(int32_t tag, void *data);

void Interrogation_unloadStateCallback();

int32_t CreditsScreen_initStateCallback(int32_t tag);

void CreditsScreen_initialPaintCallback();

void CreditsScreen_repaintCallback();

int32_t CreditsScreen_tickCallback(int32_t tag, void *data);

void CreditsScreen_unloadStateCallback();

int32_t GameMenu_initStateCallback(int32_t tag);

void GameMenu_initialPaintCallback();

void GameMenu_repaintCallback();

int32_t GameMenu_tickCallback(int32_t tag, void *data);

void GameMenu_unloadStateCallback();

int countLines();
void enterState( enum EGameMenuState State );

#define MENU_ITEM_TIME_TO_BECOME_ACTIVE_MS 200

/* 84ms * 6 blinks == ~500ms */
#define MENU_ITEM_TIME_TO_BLINK_MS 84
#endif
