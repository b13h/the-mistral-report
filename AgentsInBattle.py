from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse


class Position:
    x = 0
    y = 0


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    positions = {
        "-1": Position()
    }

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        path = urlparse(self.path).path
        query = urlparse(self.path).query

        if "set_position" in path:
            query_components = dict(qc.split("=") for qc in query.split("&"))
            id = query_components["id"]
            x = query_components["x"]
            y = query_components["y"]
            pos = Position()
            pos.x = int(x)
            pos.y = int(y)
            self.positions[id] = pos
            self.wfile.write(bytearray(id + " " + x + " " + y, "utf-8"))

        if "players" in path:
            newId = len(self.positions)
            pos = Position()
            self.positions[newId] = pos
            self.wfile.write(bytearray(str(newId), "utf-8"))

        if "positions" in path:
            response = ""

            for key in self.positions.keys():
                pos = self.positions[key]
                response = response + str(key) + " " + str(pos.x) + " " + str(pos.y) + "\n"

            self.wfile.write(bytearray(response, "utf-8"))


httpd = HTTPServer(('0.0.0.0', 8000), SimpleHTTPRequestHandler)
httpd.serve_forever()
