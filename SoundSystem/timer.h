#ifndef TIMER_H_
#define TIMER_H_
extern "C" void timer_setup(unsigned short frequency);

extern "C" void timer_shutdown();

extern "C" unsigned long timer_get();

extern "C" void timer_reset(unsigned short frequency);

extern "C" void hlt(void);

#endif
