/*
 Created by Daniel Monteiro on 18/11/2019.
*/

#ifndef THE_MISTRAL_REPORT_SOUNDSYSTEM_H
#define THE_MISTRAL_REPORT_SOUNDSYSTEM_H
void playSound( const int action );
void soundTick();
#endif /*THE_MISTRAL_REPORT_SOUNDSYSTEM_H*/
