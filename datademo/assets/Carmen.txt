Carmen is a very skilled infiltrator,
with skills on many kinds of corporal
fighting mastery.

Recognizable by her copper-coloured
hair, very light complexion and slim
figure.