#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#ifdef AMIGA
#include "AmigaInt.h"
#else

#ifdef WIN32
#include "Win32Int.h"
#else
#include <stdint.h>
#include <unistd.h>

#endif
#endif

#include "Enums.h"
#include "Common.h"
#include "FixP.h"
#include "Vec.h"
#include "MapWithCharKey.h"
#include "VisibilityStrategy.h"
#include "CActor.h"
#include "Common.h"
#include "LoadBitmap.h"
#include "CTile3DProperties.h"
#include "EDirection_Utils.h"
#include "CRenderer.h"
#include "Engine.h"
#include "FixP.h"
#include "MapWithCharKey.h"
#include "Vec.h"

int isValid(const struct Vec2i pos) {
	return 0 <= pos.x && pos.x < MAP_SIZE && 0 <= pos.y && pos.y < MAP_SIZE;
}

struct Vec2i transform(const enum EDirection from, const struct Vec2i currentPos) {

	struct Vec2i toReturn;

	switch (from) {
		case kNorth:
			toReturn = currentPos;
			break;
		case kSouth:
			initVec2i(&toReturn, (int8_t) (MAP_SIZE - currentPos.x - 1),
					  (int8_t) (MAP_SIZE - currentPos.y - 1));
			break;

		case kEast:
			initVec2i(&toReturn, (int8_t) (MAP_SIZE - currentPos.y - 1),
					  (int8_t) (MAP_SIZE - currentPos.x - 1));
			break;
		case kWest:
			initVec2i(&toReturn, (int8_t) (currentPos.y),
					  (int8_t) (currentPos.x));
			break;
		default:
			assert (FALSE);
			break;
	}

	return toReturn;
}

void castVisibility(const enum EDirection from,
					enum EVisibility *  __restrict__ visMap,
					const uint8_t *  __restrict__ occluders,
					const struct Vec2i pos,
					struct Vec2i *  __restrict__ distances,
					const int cleanPrevious,
					const struct MapWithCharKey *  __restrict__ occluderTiles) {

	const struct Vec2i originalPos = transform(from, pos);
	struct Vec2i positions[MAP_SIZE + MAP_SIZE];
	struct Vec2i currentPos;

	/* The -1 is due to the fact I will add a new element. */
	struct Vec2i *stackHead = &positions[0];
	struct Vec2i *stackEnd = stackHead + (MAP_SIZE + MAP_SIZE);
	struct Vec2i *stackRoot = stackHead;

	struct Vec2i rightOffset = mapOffsetForDirection(kEast);
	struct Vec2i leftOffset = mapOffsetForDirection(kWest);
	struct Vec2i northOffset = mapOffsetForDirection(kNorth);
	uint8_t bucketPositions[MAP_SIZE + MAP_SIZE];

	if (cleanPrevious) {
		memset (visMap, 0, sizeof(kInvisible) * MAP_SIZE * MAP_SIZE);
	}

	*stackHead = originalPos;
	++stackHead;

	memset (distances, -128, sizeof(struct Vec2i) * 2 * MAP_SIZE * MAP_SIZE);
	memset (&bucketPositions, 0, sizeof(uint8_t) * (MAP_SIZE + MAP_SIZE));

	while (stackHead != stackRoot) {
		struct Vec2i transformed;
		int verticalDistance;
		int manhattanDistance;
		int narrowing;

		--stackHead;

		currentPos = *stackHead;

		transformed = transform(from, currentPos);

		if (!isValid(transformed)) {
			continue;
		}

		if (visMap[(transformed.y * MAP_SIZE) + transformed.x] == kVisible) {
			continue;
		}

		visMap[(transformed.y * MAP_SIZE) + transformed.x] = kVisible;

		verticalDistance = (currentPos.y - originalPos.y);

		manhattanDistance =
				abs(verticalDistance) + abs(currentPos.x - originalPos.x);

#ifdef CD32
		if (manhattanDistance < (MAP_SIZE / 2)) {
#else
		if (manhattanDistance < (2 * MAP_SIZE)) {
#endif
			distances[(manhattanDistance * (MAP_SIZE))
					  + (bucketPositions[manhattanDistance]++)] = transformed;
		}

		if (getFromMap(
				occluderTiles,
				occluders[(transformed.y * MAP_SIZE) + transformed.x])) {
			continue;
		}

		narrowing = abs(verticalDistance);

		if (((currentPos.x - originalPos.x) >= -narrowing)
			&& (currentPos.x - originalPos.x) <= 0
			&& (stackHead != stackEnd)) {
			initVec2i(stackHead++, (int8_t) (currentPos.x + leftOffset.x),
					  (int8_t) (currentPos.y + leftOffset.y));
		}

		if (((currentPos.x - originalPos.x) <= narrowing)
			&& (currentPos.x - originalPos.x) >= 0
			&& (stackHead != stackEnd)) {
			initVec2i(stackHead++, (int8_t) (currentPos.x + rightOffset.x),
					  (int8_t) (currentPos.y + rightOffset.y));
		}

		if (verticalDistance <= 0 && (stackHead != stackEnd)) {
			initVec2i(stackHead++, (int8_t) (currentPos.x + northOffset.x),
					  (int8_t) (currentPos.y + northOffset.y));
		}
	}
}
