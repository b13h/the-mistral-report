#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#ifdef AMIGA
#include "AmigaInt.h"
#else

#ifdef WIN32
#include "Win32Int.h"
#else
#include <stdint.h>
#include <unistd.h>
#endif

#endif

#include "FixP.h"
#include "Vec.h"
#include "Enums.h"
#include "CActor.h"
#include "MapWithCharKey.h"
#include "Common.h"
#include "LoadBitmap.h"
#include "CTile3DProperties.h"
#include "CRenderer.h"
#include "VisibilityStrategy.h"
#include "CPackedFileReader.h"

/*	Div(intToFix(1), intToFix(16));*/
#define WALKING_BIAS 4096

int gunSpeedY = 0;
int hasSnapshot = FALSE;
int gunSpeedX = 0;
FixP_t playerHeight = 0;
FixP_t walkingBias = 0;
FixP_t playerHeightChangeRate = 0;
FixP_t playerHeightTarget = 0;
int cursorX = -1;
int cursorZ = -1;
#define MUZZLE_FLASH_TIME_IN_MS 5
int gunPositionY = YRES / 2;
int gunPositionX = XRES / 4;
int gunTargetPositionX;
int gunTargetPositionY;
int showMuzzleFlashSpriteTime = MUZZLE_FLASH_TIME_IN_MS;
int grabbingDisk = FALSE;

void hideGun() {
	gunTargetPositionY = ((2 * YRES) / 3) - 1;
	showMuzzleFlashSpriteTime = 0;
}

void shootGun() {
	gunTargetPositionY = YRES / 2;
	gunTargetPositionX = gunPositionX;
	gunPositionX = gunPositionX - 8;

	if (gunPositionX < 0) {
		gunPositionX = 0;
	}

	gunPositionY = ((2 * YRES) / 3) - 1;
}

void grabDisk() {
	grabbingDisk = TRUE;
	gunPositionY = YRES / 2;
	gunTargetPositionY = 128;
}

void showGun(const int showMuzzleFlash) {
	grabbingDisk = FALSE;
	showMuzzleFlashSpriteTime =
			(showMuzzleFlash && cursorX != -1) ? MUZZLE_FLASH_TIME_IN_MS : 0;
	gunTargetPositionY = YRES / 2;
}

int covered = FALSE;
extern int shouldDrawLights;
int useDither = TRUE;
int visibilityCached = FALSE;
int needsToRedrawVisibleMeshes = TRUE;
struct MapWithCharKey occluders;
struct MapWithCharKey colliders;
struct MapWithCharKey enemySightBlockers;
uint8_t *visibleElementsMap;
struct Bitmap *defaultFont;
uint8_t framebuffer[320 * 200];
enum EActorsSnapshotElement mActors[40][40];
enum EItemsSnapshotElement mItems[40][40];
enum EItemsSnapshotElement mEffects[40][40];
enum EDirection cameraDirection;
int playerHealth = 0;
struct Vec3 mCamera;
long gameTicks = 0;

#ifdef CD32
int distanceForPenumbra = 8;
int distanceForDarkness = 16;
#else
int distanceForPenumbra = 16;
int distanceForDarkness = 32;
#endif

int playerAmmo = 0;

#ifdef SDLSW
int mSlow = FALSE;
#endif
int linesOfSight[40][40];
int revealed[40][40];
struct Bitmap *backdrop = NULL;
struct MapWithCharKey tileProperties;
struct Vec2i cameraPosition;
int shouldShowDamageHighlight = 0;
int shouldShowDetectedHighlight = 0;
int highlightDisplayTime = 0;
uint32_t palette[256];
uint8_t texturesUsed = 0;
enum ECommand mBufferedCommand = kCommandNone;
struct Texture *nativeTextures[TOTAL_TEXTURES];

struct Projection projectionVertices[8];

struct Bitmap *foe0;
struct Bitmap *foe1;
struct Bitmap *hostage;
struct Bitmap *foeBack;
struct Bitmap *target;
struct Bitmap *clue;
struct Bitmap *barrel;
struct Bitmap *deadFoe;
struct Bitmap *pistol;
struct Bitmap *diskInHand;
struct Bitmap *blood;
struct Bitmap *detectedAlert;
struct Bitmap *muzzleFlash;
struct Bitmap *myMuzzleFlash;

enum EVisibility visMap[MAP_SIZE * MAP_SIZE];
struct Vec2i distances[2 * MAP_SIZE * MAP_SIZE];

void loadTileProperties(const uint8_t levelNumber) {
	char buffer[64];
	uint8_t *data;
	int c;

	clearMap(&tileProperties);
	clearMap(&occluders);
	clearMap(&colliders);
	clearMap(&enemySightBlockers);

	gunTargetPositionX = gunPositionX;
	gunTargetPositionY = gunPositionY;

	sprintf (buffer, "props%d.bin", levelNumber);

	data = loadBinaryFileFromPath(buffer);
	loadPropertyList(&buffer[0], &tileProperties);

	for (c = 0; c < 256; ++c) {
		struct CTile3DProperties *prop =
				(struct CTile3DProperties *) getFromMap(&tileProperties, c);

		if (prop) {

			if (prop->mBlockVisibility) {
				setInMap(&occluders, c, &occluders);
			}

			setInMap(&enemySightBlockers, c,
					 prop->mBlockEnemySight ? &enemySightBlockers : NULL);
		} else {
			setInMap(&occluders, c, NULL);
			setInMap(&enemySightBlockers, c, NULL);
		}
	}

	free(data);
}

void loadTexturesForLevel(const uint8_t levelNumber) {
	char tilesFilename[64];
	uint8_t *data;
	size_t size;
	char *head;
	char *end;
	char *nameStart;

	sprintf (tilesFilename, "tiles%d.lst", levelNumber);

	data = loadBinaryFileFromPath(tilesFilename);
	size = sizeOfFile(tilesFilename);
	head = (char *) data;
	end = head + size;
	nameStart = head;

	texturesUsed = 0;
	clearTextures();

	while (head != end && (texturesUsed < TOTAL_TEXTURES)) {
		char val = *head;
		if (val == '\n' || val == 0) {
			*head = 0;
			nativeTextures[texturesUsed] = (makeTextureFrom(nameStart));
			nameStart = head + 1;
			texturesUsed++;
		}
		++head;
	}

	free(data);

	target = loadBitmap("target.img");
	foe0 = loadBitmap("enemy0.img");
	foe1 = loadBitmap("enemy1.img");
	foeBack = loadBitmap("enemyb.img");
	barrel = loadBitmap("barrel.img");
	deadFoe = loadBitmap("enemyd.img");
	clue = loadBitmap("clue.img");
	hostage = loadBitmap("hostage.img");
	blood = loadBitmap("blood.img");
	detectedAlert = loadBitmap("detected.img");
	pistol = loadBitmap("pistol.img");
	diskInHand = loadBitmap("handdisk.img");
	muzzleFlash = loadBitmap("muzzle.img");
	myMuzzleFlash = loadBitmap("myflash.img");
	backdrop = loadBitmap("backdrop.img");
}

void updateCursorForRenderer(const int x, const int z) {
	needsToRedrawVisibleMeshes = TRUE;
	visibilityCached = FALSE;
	cursorX = x;
	cursorZ = z;

	if (x == -1) {
		gunTargetPositionX = XRES / 4;
	}
}

void drawMap(const uint8_t * __restrict__ elements,
			 const uint8_t * __restrict__ items,
			 const uint8_t * __restrict__ actors,
			 uint8_t * __restrict__ effects,
			 const struct CActor *current) {

	int8_t z, x;
	const struct Vec2i mapCamera = current->mPosition;
	cameraDirection = current->mDirection;
	visibleElementsMap = elements;
	hasSnapshot = TRUE;

	if (visibilityCached) {
		return;
	}

	visibilityCached = TRUE;
	needsToRedrawVisibleMeshes = TRUE;

	cameraPosition = mapCamera;
	uint16_t offset;
	for (z = 0; z < MAP_SIZE; ++z) {
		for (x = 0; x < MAP_SIZE; ++x) {
			const offset = (MAP_SIZE * z) + x;
			const uint8_t actor = actors[offset];
			const uint8_t item = items[offset];
			const uint8_t effect = effects[offset];

			mActors[z][x] = kNobody;
			mItems[z][x] = kNoItem;
			mEffects[z][x] = kNoItem;

			if (actor != '.') {

				if (actor != current->mView) {
					if (actor == 'e') {
						mActors[z][x] = kEnemy0;
					} else if (actor == 'f') {
						mActors[z][x] = kEnemy1;
					} else if (actor == 'g') {
						mActors[z][x] = kEnemyBack;

					} else {
						mActors[z][x] = kNobody;
					}
				}
			} else {
				mActors[z][x] = kNobody;
			}

			if (item != '.') {
				if (item == 'b') {
					mItems[z][x] = kBarrel;
				} else if (item == '*') {
					mItems[z][x] = kDeadEnemy;
				} else if (item == '?') {
					mItems[z][x] = kHostage;
				} else if (item == 'v' || item == 'K' || item == 'i') {
					mItems[z][x] = kClue;
				}
			}

			if (effect == '+') {
				mEffects[z][x] = kFlash;
			}

			if (effect != '.') {
				effects[(MAP_SIZE * z) + x] = '.';
			}
		}
	}

	switch (cameraDirection) {
		case kNorth:
			mCamera.mX = intToFix(78 - (2 * cameraPosition.x));
			mCamera.mZ = intToFix((2 * cameraPosition.y) - 79);
			break;

		case kSouth:
			mCamera.mX = intToFix((2 * cameraPosition.x));
			mCamera.mZ = intToFix(-2 * cameraPosition.y - 1);
			break;

		case kWest:
			mCamera.mX = intToFix((2 * cameraPosition.y));
			mCamera.mZ = intToFix((2 * cameraPosition.x) - 1);
			break;

		case kEast:
			mCamera.mX = intToFix(-(2 * cameraPosition.y));
			mCamera.mZ = intToFix(79 - (2 * cameraPosition.x));
			break;
	}

#ifndef FIX16
	if ((cameraPosition.x + cameraPosition.y) & 1) {
		walkingBias = WALKING_BIAS;
	} else {
		walkingBias = 0;
	}
#else
	walkingBias = 0;
#endif

	castVisibility(cameraDirection, visMap, &elements[0], cameraPosition,
				   distances, TRUE, &occluders);

	++gameTicks;
}

enum ECommand getInput() {
	const enum ECommand toReturn = mBufferedCommand;
	mBufferedCommand = kCommandNone;
	return toReturn;
}

void render(const long ms) {
	static FixP_t zero = 0;
	FixP_t two = intToFix(2);
	FixP_t four = intToFix(4);
	FixP_t one = intToFix(1);
	const FixP_t halfOne = Div(one, two);
	FixP_t standardHeight = Div(intToFix(165), intToFix(100));

	if (!hasSnapshot) {
		return;
	}

	if (playerHeight < playerHeightTarget) {
		playerHeight += playerHeightChangeRate;
	}

	if (gunTargetPositionY != gunPositionY) {
		gunSpeedY = (gunTargetPositionY > gunPositionY) ? 2 : -2;
		gunPositionY += gunSpeedY;
		needsToRedrawVisibleMeshes = TRUE;
	}

	if (gunTargetPositionX != gunPositionX) {
		gunSpeedX = (gunTargetPositionX > gunPositionX) ? 2 : -2;
		gunPositionX += gunSpeedX;
		needsToRedrawVisibleMeshes = TRUE;
	}

	if (needsToRedrawVisibleMeshes) {
		enum EActorsSnapshotElement actorsSnapshotElement = kNobody;
		enum EItemsSnapshotElement itemsSnapshotElement = kNoItem;
		enum EItemsSnapshotElement effectsSnapshotElement = kNoItem;
		char buffer[64];
		char directions[4] = {'N', 'E', 'S', 'W'};
		struct Vec3 tmp;
		struct CTile3DProperties *tileProp;
		FixP_t heightDiff;
		uint8_t lastElement = 0xFF;
		uint8_t element = 0;
		int onTarget = FALSE;
		struct Vec3 position;
		FixP_t tileHeight = 0;
		int16_t x, y, z;
		int distance;
		FixP_t cameraHeight;
		int c;
		uint8_t facesMask;

		highlightDisplayTime -= ms;
		needsToRedrawVisibleMeshes = FALSE;

#ifdef SDLSW
		clearRenderer();
#endif

#ifndef CD32
		for (c = 0; c < (256 / 32); ++c) {
			drawBitmap(c * 32, 0, backdrop, FALSE);
		}
		fill(0, 64, 256, 64, 0, FALSE);
#else
		fill(0, 0, 256, 128, 64, FALSE);
#endif

		element = visibleElementsMap[(cameraPosition.y * MAP_SIZE) + cameraPosition.x];

		tileProp = ((struct CTile3DProperties *) getFromMap(&tileProperties,
															element));

		if (tileProp) {
			tileHeight = tileProp->mFloorHeight;
		}

		cameraHeight = -2 * tileHeight;

		mCamera.mY = cameraHeight - standardHeight;

		for (distance = (MAP_SIZE + MAP_SIZE - 1); distance >= 0; --distance) {
			uint8_t bucketPos;

			for (bucketPos = 0; bucketPos < MAP_SIZE; ++bucketPos) {

				struct Vec2i visPos = distances[(distance * MAP_SIZE) + bucketPos];

				x = 0;
				z = 0;

				if (visPos.x < 0 || visPos.y < 0 || visPos.x >= MAP_SIZE
					|| visPos.y >= MAP_SIZE) {
					bucketPos = MAP_SIZE;
					continue;
				}

				revealed[visPos.y][visPos.x] = TRUE;

				facesMask = MASK_LEFT | MASK_FRONT | MASK_RIGHT;

				switch (cameraDirection) {

					case kNorth:
						x = visPos.x;
						z = visPos.y;
						onTarget = (cursorX == x && cursorZ == z);
						element = visibleElementsMap[ (z * MAP_SIZE) + x];

						actorsSnapshotElement = mActors[z][x];
						itemsSnapshotElement = mItems[z][x];
						effectsSnapshotElement = mEffects[z][x];

						position.mX =
								mCamera.mX + intToFix(-2 * ((MAP_SIZE - 1) - x));
						position.mY = mCamera.mY;
						position.mZ =
								mCamera.mZ + intToFix(2 * (MAP_SIZE) - (2 * z));

						if (x > 0) {
							facesMask |= (visibleElementsMap[(z * MAP_SIZE) + (x - 1)] != element) ?
										 MASK_RIGHT :
										 0;
						}

						/* remember, bounds - 1! */
						if ((x < (MAP_SIZE - 1)) && (visibleElementsMap[(z * MAP_SIZE) + (x + 1)] == element) ) {
							facesMask &= ~MASK_LEFT;
						}

						if ((z < (MAP_SIZE - 1)) && (visibleElementsMap[((z + 1) * MAP_SIZE) + x] == element) ) {
							facesMask &= ~MASK_FRONT;
						}

						if (z == cameraPosition.y - 1) {
							if (getFromMap(&occluders, element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}

						break;

					case kSouth:
						x = visPos.x;
						z = visPos.y;

						element = visibleElementsMap[(z * MAP_SIZE) + x];
						onTarget = (cursorX == x && cursorZ == z);
						actorsSnapshotElement = mActors[z][x];
						itemsSnapshotElement = mItems[z][x];
						effectsSnapshotElement = mEffects[z][x];

						position.mX = mCamera.mX + intToFix(-2 * x);
						position.mY = mCamera.mY;
						position.mZ = mCamera.mZ + intToFix(2 + 2 * z);

						/*						remember, bounds - 1!*/

						if ((x > 0) && (visibleElementsMap[(z * MAP_SIZE) + (x - 1)] == element) ) {
							facesMask &= ~MASK_LEFT;
						}

						if ((x < (MAP_SIZE - 1)) && (visibleElementsMap[(z * MAP_SIZE) + (x + 1)] == element) ) {
							facesMask &= ~MASK_RIGHT;
						}
						/*
										if (z < 0) {
											facesMask[1] = (visibleElementsMap[(z - 1)][x] !=
															element);
										}
				*/
						if (z == (cameraPosition.y) + 1) {

							if (getFromMap(&occluders, element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}

						break;
					case kWest:
						x = visPos.y;
						z = visPos.x;

						element = visibleElementsMap[(x * MAP_SIZE) + z];
						onTarget = (cursorX == z && cursorZ == x);
						itemsSnapshotElement = mItems[x][z];
						effectsSnapshotElement = mEffects[x][z];
						actorsSnapshotElement = mActors[x][z];

						position.mX = mCamera.mX + intToFix(-2 * x);
						position.mY = mCamera.mY;
						position.mZ = mCamera.mZ + intToFix(2 - 2 * z);

						/* remember, bounds - 1! */

						if ((x > 0) && (visibleElementsMap[((x - 1) * MAP_SIZE) + z] == element)) {
							facesMask &= ~MASK_LEFT;
						}

						if ((x < (MAP_SIZE - 1)) && (visibleElementsMap[((x + 1) * MAP_SIZE) + z] == element)) {
							facesMask &= ~MASK_RIGHT;
						}

						if ((z < (MAP_SIZE - 1)) && (visibleElementsMap[(x * MAP_SIZE) + (z + 1)] == element)) {
							facesMask &= ~MASK_FRONT;
						}

						if (z == (cameraPosition.x) - 1) {

							if (getFromMap(&occluders, element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}
						break;

					case kEast:
						x = visPos.y;
						z = visPos.x;

						element = visibleElementsMap[(x * MAP_SIZE) + z];
						onTarget = (cursorX == z && cursorZ == x);
						actorsSnapshotElement = mActors[x][z];
						itemsSnapshotElement = mItems[x][z];
						effectsSnapshotElement = mEffects[x][z];

						position.mX = mCamera.mX + intToFix(2 * x);
						position.mY = mCamera.mY;
						position.mZ = mCamera.mZ + intToFix(2 * (z - MAP_SIZE + 1));


						/* remember, bounds - 1! */
						if ((x > 0) && (visibleElementsMap[((x - 1) * MAP_SIZE) + z] == element) ) {
							facesMask &= ~MASK_RIGHT;
						}

						if ((x < (MAP_SIZE - 1)) && (visibleElementsMap[((x + 1) * MAP_SIZE) + z] == element)) {
							facesMask &= ~MASK_LEFT;
						}

						if ((z < (MAP_SIZE - 1)) && (visibleElementsMap[(x * MAP_SIZE) + (z - 1)] == element)) {
							facesMask &= ~MASK_FRONT;
						}

						if (z == (cameraPosition.x) + 1) {

							if (getFromMap(&occluders, element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}
						break;
					default:
						assert (FALSE);
				}

				if (lastElement != element) {
					tileProp = (struct CTile3DProperties *) getFromMap(&tileProperties,
																	   element);
				}

				if (tileProp == NULL) {
					continue;
				}

				heightDiff = tileProp->mCeilingHeight - tileProp->mFloorHeight;
				lastElement = element;

				if (tileProp->mFloorRepeatedTextureIndex != 0xFF
					&& tileProp->mFloorRepetitions > 0) {

					switch (tileProp->mGeometryType) {
						case kRightNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mFloorHeight * 2)
									   - intToFix(tileProp->mFloorRepetitions)),
									  zero);

							drawRightNear(
									tmp, intToFix(tileProp->mFloorRepetitions),
									nativeTextures[tileProp->mFloorRepeatedTextureIndex]
											->rowMajor,
									facesMask, TRUE);

							break;

						case kLeftNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mFloorHeight * 2)
									   - intToFix(tileProp->mFloorRepetitions)),
									  zero);

							drawLeftNear(
									tmp, intToFix(tileProp->mFloorRepetitions),
									nativeTextures[tileProp->mFloorRepeatedTextureIndex]
											->rowMajor, facesMask, TRUE);
							break;

						case kCube:
						default:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mFloorHeight * 2)
									   - intToFix(tileProp->mFloorRepetitions)),
									  zero);

							drawColumnAt(
									tmp, intToFix(tileProp->mFloorRepetitions),
									nativeTextures[tileProp->mFloorRepeatedTextureIndex],
									facesMask, FALSE, TRUE);
							break;
					}
				}

				if (tileProp->mCeilingRepeatedTextureIndex != 0xFF
					&& tileProp->mCeilingRepetitions > 0) {

					switch (tileProp->mGeometryType) {
						case kRightNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mCeilingHeight * 2)
									   + intToFix(tileProp->mCeilingRepetitions)),
									  zero);

							drawRightNear(
									tmp, intToFix(tileProp->mCeilingRepetitions),
									nativeTextures[tileProp->mCeilingRepeatedTextureIndex]
											->rowMajor,
									facesMask, TRUE);
							break;

						case kLeftNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mCeilingHeight * 2)
									   + intToFix(tileProp->mCeilingRepetitions)),
									  zero);

							drawLeftNear(
									tmp, intToFix(tileProp->mCeilingRepetitions),
									nativeTextures[tileProp->mCeilingRepeatedTextureIndex]
											->rowMajor,
									facesMask, TRUE);
							break;

						case kCube:
						default:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mCeilingHeight * 2)
									   + intToFix(tileProp->mCeilingRepetitions)),
									  zero);

							drawColumnAt(
									tmp, intToFix(tileProp->mCeilingRepetitions),
									nativeTextures[tileProp->mCeilingRepeatedTextureIndex],
									facesMask, FALSE, TRUE);
							break;
					}
				}

				if (tileProp->mFloorTextureIndex != 0xFF) {

					tmp.mX = position.mX;
					tmp.mY = position.mY;
					tmp.mZ = position.mZ;

					addToVec3(&tmp, 0, (tileProp->mFloorHeight * 2), 0);



					drawFloorAt(tmp, nativeTextures[tileProp->mFloorTextureIndex], cameraDirection);
				}

				if (tileProp->mCeilingTextureIndex != 0xFF) {

					uint8_t newDirection = cameraDirection;

					tmp.mX = position.mX;
					tmp.mY = position.mY;
					tmp.mZ = position.mZ;

					addToVec3(&tmp, 0, (tileProp->mCeilingHeight * 2), 0);

					if (cameraDirection == kNorth) {
						newDirection = kSouth;
					} if (cameraDirection == kSouth) {
						newDirection = kNorth;
					}

					drawCeilingAt(
							tmp, nativeTextures[tileProp->mCeilingTextureIndex], newDirection);
				}

				if (tileProp->mGeometryType != kNoGeometry
					&& tileProp->mMainWallTextureIndex != 0xFF) {

					int integerPart = fixToInt(tileProp->mCeilingHeight)
									  - fixToInt(tileProp->mFloorHeight);

					FixP_t adjust = 0;

					if (((heightDiff * 2) - intToFix(integerPart)) >= halfOne) {
						adjust = Div(halfOne, four);
					}

					switch (tileProp->mGeometryType) {
						case kRightNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mFloorHeight * 2) + heightDiff),
									  zero);

							drawRightNear(
									tmp, (heightDiff + Div(adjust, two)),
									nativeTextures[tileProp->mMainWallTextureIndex]->rowMajor,
									facesMask, tileProp->mRepeatMainTexture);
							break;

						case kLeftNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mFloorHeight * 2) + heightDiff),
									  zero);

							drawLeftNear(
									tmp, (heightDiff + Div(adjust, two)),
									nativeTextures[tileProp->mMainWallTextureIndex]->rowMajor,
									facesMask, tileProp->mRepeatMainTexture);
							break;

						case kCube:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, zero,
									  ((tileProp->mFloorHeight * 2) + heightDiff),
									  zero);

							drawColumnAt(tmp, (heightDiff + Div(adjust, two)),
										 nativeTextures[tileProp->mMainWallTextureIndex],
										 facesMask, tileProp->mNeedsAlphaTest,
										 tileProp->mRepeatMainTexture);
						default:
							break;
					}
				}

				if (actorsSnapshotElement != kNobody) {
					struct Bitmap *sprite = NULL;
					switch (actorsSnapshotElement) {
						case kEnemy0:
							sprite = foe0;
							break;
						case kEnemy1:
							sprite = foe1;
							break;
						case kEnemyBack:
							sprite = foeBack;
							break;
						case kNobody:
						default:
							assert (FALSE);
							break;
					}

					if (sprite) {
						tmp.mX = position.mX;
						tmp.mY = position.mY;
						tmp.mZ = position.mZ;

						addToVec3(&tmp, 0, ((tileProp->mFloorHeight * 2) + one), 0);

						drawBillboardAt(tmp, sprite->data, one, sprite->width);
					}

					if (onTarget) {
						int original = shouldDrawLights;
						shouldDrawLights = FALSE;

						gunTargetPositionX =
								(XRES / 4) + (fixToInt(position.mX) * 4);

						if (gunTargetPositionX < 0 || gunTargetPositionX > XRES) {

							gunTargetPositionX = XRES / 4;
							hideGun();
						}

						tmp.mX = position.mX;
						tmp.mY = position.mY;
						tmp.mZ = position.mZ;

						addToVec3(&tmp, 0,
								  (tileProp->mFloorHeight * 2) + standardHeight, 0);

						drawBillboardAt(tmp, target->data, one, 32);

						shouldDrawLights = original;
					}
				}

				if (itemsSnapshotElement != kNoItem) {
					switch (itemsSnapshotElement) {
						case kClue:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, 0, (tileProp->mFloorHeight * 2) + one, 0);

							drawBillboardAt(tmp, clue->data, one, 32);
							break;

						case kBarrel:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, 0, (tileProp->mFloorHeight * 2) + one, 0);

							drawBillboardAt(tmp, barrel->data, one, 32);
							break;

						case kHostage:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, 0,
									  (tileProp->mFloorHeight * 2) + Div(one, four)
									  + Div(one, two),
									  0);

							drawBillboardAt(tmp, hostage->data, one, hostage->width);
							break;

						case kDeadEnemy:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, 0,
									  (tileProp->mFloorHeight * 2) + Div(one, four)
									  + Div(one, two),
									  0);

							drawBillboardAt(tmp, deadFoe->data, one, deadFoe->width);
							break;

						case kNoItem:
							break;

						case kFlash:
						default:
							assert (FALSE);
							break;
					}
				}

				if (effectsSnapshotElement != kNoItem) {
					switch (effectsSnapshotElement) {
						case kFlash:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							addToVec3(&tmp, 0,
									  ((tileProp->mFloorHeight * 2) + one + halfOne),
									  0);

							drawBillboardAt(tmp, muzzleFlash->data, one, 32);
							break;
						case kNoItem:
							break;
						default:
						case kBarrel:
							assert (FALSE);
							break;
					}
				}
			}
		}

		if (gunTargetPositionY == gunPositionY) {
			if (grabbingDisk) {
				showGun(FALSE);
				grabbingDisk = FALSE;
			}
			gunSpeedY = 0;
		}

		if (gunTargetPositionX == gunPositionX) {
			gunSpeedX = 0;
		}

		clippingY1 = 128;
		if (!grabbingDisk) {
			if (showMuzzleFlashSpriteTime) {
				showMuzzleFlashSpriteTime -= ms;

				needsToRedrawVisibleMeshes = TRUE;

				drawBitmap(gunPositionX + pistol->width / 2,
						   gunPositionY - (pistol->height / 3), myMuzzleFlash,
						   TRUE);
			}

			drawBitmap(gunPositionX, gunPositionY, pistol, TRUE);
		} else {
			drawBitmap(gunPositionX, gunPositionY, diskInHand, TRUE);
		}

		clippingY1 = 200;

		if (shouldShowDamageHighlight) {

			drawBitmap(0, 0, blood, TRUE);

			if (highlightDisplayTime <= 0) {
				shouldShowDamageHighlight = FALSE;
			}

			needsToRedrawVisibleMeshes = TRUE;
		}

		if (shouldShowDetectedHighlight) {

			drawBitmap(0, 0, detectedAlert, TRUE);

			if (highlightDisplayTime <= 0) {
				shouldShowDetectedHighlight = FALSE;
			}
			needsToRedrawVisibleMeshes = TRUE;
		}

		drawRect(0, 0, 255, 128, 0);

		drawRect(256, 0, 64, 128, 0);

		fill(0, 0, 320, 8, 0, FALSE);
		drawTextAt(2, 1, "Agent in the field", 255);

		drawTextAt(34, 1, "Map", 255);

		fill(256, 8, 320 - 256, 128 - 8, 255, FALSE);

		drawRect(256 + 12, 16, 320 - 256 - 24, 80, 0);

		for (y = 0; y < 40; ++y) {
			for (x = 0; x < 40; ++x) {
				uint8_t tile = visibleElementsMap[(y * MAP_SIZE) + x];

				if (!revealed[y][x]) {
					linesOfSight[y][x] = FALSE;
					continue;
				}

				fill(256 + 12 + (x), 16 + (2 * y), 1, 2, 64, FALSE);

				if (getFromMap(&colliders, tile)) {
					fill(256 + 12 + (x), 16 + (2 * y), 1, 2, 0, FALSE);
				}

				if (linesOfSight[y][x]) {
					fill(256 + 12 + (x), 16 + (2 * y), 1, 2, 128, FALSE);
				}

				if (mActors[y][x] != kNobody) {
					fill(256 + 12 + (x), 16 + (2 * y), 1, 2, 192, FALSE);
				}

				if (cursorZ == y && x == cursorX) {
					fill(256 + 12 + (x), 16 + (2 * y), 1, 2, 255, FALSE);
				}

				if (tile == 'K' || tile == 'E' || tile == 'i') {
					fill(256 + 12 + (x), 16 + (2 * y), 1, 2, 128, FALSE);
				}

				linesOfSight[y][x] = FALSE;
			}
		}

		fill(256 + 12, 16 + (2 * cameraPosition.y), (cameraPosition.x), 1,
			 (cameraDirection == kWest) ? 48 : 32, FALSE);

		fill(256 + 12 + (cameraPosition.x), 16 + (2 * cameraPosition.y),
			 320 - (256 + 24 + (cameraPosition.x)), 1,
			 (cameraDirection == kEast) ? 48 : 32, FALSE);

		fill(256 + 12 + (cameraPosition.x), 16, 1, (2 * cameraPosition.y),
			 (cameraDirection == kNorth) ? 48 : 32, FALSE);

		fill(256 + 12 + (cameraPosition.x), 16 + (2 * cameraPosition.y), 1,
			 96 - (16 + (2 * cameraPosition.y)),
			 (cameraDirection == kSouth) ? 48 : 32, FALSE);

		fill(256 + 12 + (cameraPosition.x), 16 + (2 * cameraPosition.y), 1,
			 2, 48, FALSE);

		fill(0, 144, 312, 48, 255, FALSE);
		drawRect(0, 144, 312, 48, 0);

		sprintf (buffer, "HP: %d Ammo: %d", playerHealth, playerAmmo);
		drawTextAt(2, 20, buffer, 15);
		sprintf (buffer, "Dir: %c", directions[(int) (cameraDirection)]);

		fill(256, 15 * 8, 8 * 7, 8, 255, FALSE);

		drawTextAt(34, 16, buffer, 0);
		if (covered) {
			drawTextAt(34, 15, "Covered", 15);
		}
	}
}
