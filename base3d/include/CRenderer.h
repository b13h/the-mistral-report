#ifndef RENDERER_H
#define RENDERER_H

#ifndef CD32
#define NATIVE_TEXTURE_SIZE 32
#else
#define NATIVE_TEXTURE_SIZE 16
#endif

#define XRES 256
#define YRES 128
#define HALF_XRES 128
#define HALF_YRES 64
#define TOTAL_TEXTURES 32
#define TRANSPARENCY_COLOR 199

struct Projection {
	struct Vec3 first;
	struct Vec2 second;
};

extern int shouldDrawLights;
extern struct MapWithCharKey occluders;
extern struct MapWithCharKey enemySightBlockers;
extern struct MapWithCharKey colliders;
extern int useDither;
extern int visibilityCached;
extern int needsToRedrawVisibleMeshes;
extern uint8_t* visibleElementsMap;
extern struct Bitmap *defaultFont;
extern uint8_t framebuffer[320 * 200];
extern int linesOfSight[MAP_SIZE][MAP_SIZE];
extern int revealed[MAP_SIZE][MAP_SIZE];
extern enum EDirection cameraDirection;
extern long gameTicks;
extern int playerAmmo;
extern int playerHealth;
extern int hasSnapshot;
extern int distanceForPenumbra;
extern int distanceForDarkness;

#define MASK_LEFT 1
#define MASK_FRONT 2
#define MASK_RIGHT 4
#define MASK_BEHIND 8

extern struct MapWithCharKey tileProperties;
extern struct Vec2i cameraPosition;
extern int shouldShowDamageHighlight;
extern int shouldShowDetectedHighlight;
extern int highlightDisplayTime;
extern uint32_t palette[256];
extern uint8_t texturesUsed;
extern enum ECommand mBufferedCommand;
extern struct Texture *nativeTextures[TOTAL_TEXTURES];
extern uint16_t clippingY1;
extern int covered;
extern struct Projection projectionVertices[8];
extern FixP_t playerHeight;
extern FixP_t walkingBias;
extern FixP_t playerHeightChangeRate;
extern FixP_t playerHeightTarget;
extern int grabbingDisk;

void graphicsInit();

void graphicsShutdown();

void clearRenderer();

void flipRenderer();

void render(long ms);

void hideGun();

void shootGun();

void grabDisk();

void showGun(const int showMuzzleFlash);

void loadTexturesForLevel(const uint8_t levelNumber);

void loadTileProperties(const uint8_t levelNumber);

void updateCursorForRenderer(const int x, const int y);

enum ECommand getInput();

void handleSystemEvents();

void projectAllVertices(const uint8_t count);

uint8_t getPaletteEntry(const uint32_t origin);

void fill(
		const int16_t x, const int16_t y,
		const int16_t dx, const int16_t dy,
		const uint8_t pixel, const int stipple);

void drawMap(const uint8_t * __restrict__ elements,
			 const uint8_t * __restrict__ items,
			 const uint8_t * __restrict__ actors,
			 uint8_t * __restrict__ effects,
			 const struct CActor * __restrict__ current);

void drawTextAt(const uint16_t x,
				const uint16_t y,
				const char * __restrict__ text,
				const uint8_t colour);

void drawFloorAt(const struct Vec3 center,
				 const struct Texture * __restrict__ texture, uint8_t rotation);

void drawCeilingAt(const struct Vec3 center,
				   const struct Texture * __restrict__ texture, uint8_t rotation);

void drawLeftNear(const struct Vec3 center,
				  const FixP_t scale,
				  const uint8_t * __restrict__ texture,
				  const uint8_t mask,
				  const int repeatedTexture);

void drawRightNear(const struct Vec3 center,
				   const FixP_t scale,
				   const uint8_t * __restrict__ texture,
				   const uint8_t mask,
				   const int repeatedTexture);

void drawColumnAt(const struct Vec3 center,
				  const FixP_t scale,
				  const struct Texture * __restrict__ texture,
				  const uint8_t mask,
				  const int enableAlpha,
				  const int repeatedTexture);

void drawBillboardAt(const struct Vec3 center,
					 const uint8_t * __restrict__ texture,
					 const FixP_t scale,
					 const int size);

void drawBitmap(const int16_t x,
				const int16_t y,
				const struct Bitmap * __restrict__ tile,
						const int transparent);

void drawRepeatBitmap(
		const int16_t x,
		const int16_t y,
		const int16_t dx,
		const int16_t dy,
		const struct Bitmap * __restrict__ tile);

void drawRect(const int16_t x,
			const int16_t y,
			const uint16_t dx,
			const uint16_t dy,
			const uint8_t pixel);

void drawWall(FixP_t x0,
			  FixP_t x1,
			  FixP_t x0y0,
			  FixP_t x0y1,
			  FixP_t x1y0,
			  FixP_t x1y1,
			  const uint8_t * __restrict__ texture,
			  const FixP_t textureScaleY,
			  const int z);

void drawFloor(FixP_t y0,
			   FixP_t y1,
			   FixP_t x0y0,
			   FixP_t x1y0,
			   FixP_t x0y1,
			   FixP_t x1y1,
			   int z,
			   const uint8_t * __restrict__ texture);

void drawFrontWall(FixP_t x0,
				   FixP_t y0,
				   FixP_t x1,
				   FixP_t y1,
				   const uint8_t * __restrict__ texture,
				   const FixP_t textureScaleY,
				   const int z,
				   const int enableAlpha,
				   const int size);

void drawMask(const FixP_t x0,
			const FixP_t y0,
			const FixP_t x1,
			const FixP_t y1);

void maskWall(
		FixP_t x0,
		FixP_t x1,
		FixP_t x0y0,
		FixP_t x0y1,
		FixP_t x1y0,
		FixP_t x1y1);

void maskFloor(
		FixP_t y0,
		FixP_t y1,
		FixP_t x0y0,
		FixP_t x1y0,
		FixP_t x0y1,
		FixP_t x1y1
#ifdef FLAT_FLOOR_CEILING
		,uint8_t pixel
#endif
		);

#endif
