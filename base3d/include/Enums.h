#ifndef ENUMS_H
#define ENUMS_H

enum EActorsSnapshotElement {
	kNobody, kEnemy0, kEnemy1, kEnemyBack
};

enum EItemsSnapshotElement {
	kNoItem,
	kFlash,
	kBarrel,
	kDeadEnemy,
	kHostage,
	kClue
};

enum EDirection {
	kNorth, kEast, kSouth, kWest
};

enum CrawlerState {
	kCrawlerGameOver = -1,
	kCrawlerQuit = 0,
	kCrawlerGameInProgress = 1,
	kCrawlerClueAcquired = 2
};

enum ECommand {
	kCommandNone,
	kCommandUp,
	kCommandRight,
	kCommandDown,
	kCommandLeft,
	kCommandFire1,
	kCommandFire2,
	kCommandBack,
	kCommandQuit,
	kCommandStrafeLeft,
	kCommandStrafeRight,
	kCommandFire3,
	kCommandFire4
};

enum EGameMenuState {
	kMainMenu,
	kPracticeCrawling,
	kPlayGame,
	kGameMenu,
	kPassTurn,
	kHelp,
	kCredits,
	kQuit,
	kDossiersMenu,
	kInvestigateMenu,
	kTravelMenu,
	kStatusMenu,
	kEndGame,
	kReadDossier_Sofia,
	kReadDossier_Ricardo,
	kReadDossier_Lola,
	kReadDossier_Pau,
	kReadDossier_Lina,
	kReadDossier_Elias,
	kReadDossier_Carmen,
	kReadDossier_Jean,
	kTravelPorto,
	kTravelLisbon,
	kTravelMadrid,
	kTravelBarcelona,
	kTravelFrankfurt,
	kTravelHamburg,
	kTravelLuxembourg,
	kTravelBrussels,
	kVictory,
	kGameOver,
	kPrologue,
	kEpilogue,
	kPracticeInterrogation,
	kInterrogate_Sofia,
	kInterrogate_Ricardo,
	kInterrogate_Lola,
	kInterrogate_Pau,
	kInterrogate_Lina,
	kInterrogate_Elias,
	kInterrogate_Carmen,
	kInterrogate_Jean,
	kAccuseMenu,
	kAccuse_Sofia,
	kAccuse_Ricardo,
	kAccuse_Lola,
	kAccuse_Pau,
	kAccuse_Lina,
	kAccuse_Elias,
	kAccuse_Carmen,
	kAccuse_Jean
};

enum EPresentationState {
	kAppearing,
	kWaitingForInput,
	kConfirmInputBlink1,
	kConfirmInputBlink2,
	kConfirmInputBlink3,
	kConfirmInputBlink4,
	kConfirmInputBlink5,
	kConfirmInputBlink6,
	kFade
};

enum ESoundDriver {
	kNoSound, kPcSpeaker, kOpl2Lpt, kAdlib, kTandy, kCovox
};

#define MENU_SELECTION_CHANGE_SOUND 0
#define STATE_CHANGE_SOUND 1
#define INFORMATION_ACQUIRED_SOUND 2
#define FAILED_TO_GET_INFORMATION_SOUND 3
#define PLAYER_GOT_DETECTED_SOUND 4
#define PLAYER_FIRING_GUN 5
#define ENEMY_FIRING_GUN 6
#define PLAYER_GET_HURT_SOUND 7
#endif
