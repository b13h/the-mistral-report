#ifndef TILEPROPS_H
#define TILEPROPS_H

typedef uint8_t TextureIndex;

struct CTile3DProperties;

enum GeometryType {
	kNoGeometry, kCube, kLeftNearWall, kRightNearWall
};

struct CTile3DProperties {
	int mNeedsAlphaTest;
	int mBlockVisibility;
	int mBlockMovement;
	int mBlockEnemySight;
	int mRepeatMainTexture;
	TextureIndex mCeilingTextureIndex;
	TextureIndex mFloorTextureIndex;
	TextureIndex mMainWallTextureIndex;
	TextureIndex mCeilingRepeatedTextureIndex;
	TextureIndex mFloorRepeatedTextureIndex;
	enum GeometryType mGeometryType;
	uint8_t mCeilingRepetitions;
	uint8_t mFloorRepetitions;
	FixP_t mCeilingHeight;
	FixP_t mFloorHeight;
};

void loadPropertyList(const char * __restrict__ propertyFile, struct MapWithCharKey *map);

#endif
